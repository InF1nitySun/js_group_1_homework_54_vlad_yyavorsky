import React, {Component} from 'react';
import './App.css';
import Card from "./Card/card";
import WinCalculator from './WinCalculator';

const suits = ['D', 'H', 'C', 'S'];
const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

class App extends Component {
    state = {
        cards: [],
        outcome: ''
    };

    shuffleCards = () => {
        const deck = [];
        for (let s = 0; s < suits.length; s++) {
            for (let r = 0; r < ranks.length; r++) {
                let card = {suit: suits[s], rank: ranks[r]};
                deck.push(card);
            }
        }
        const cards = [];

        for (let c = 0; c < 5; c++) {
            let randomCardIndex = Math.floor(Math.random() * deck.length);
            let [randomCard] = deck.splice(randomCardIndex, 1);
            cards.push(randomCard);
        }
        const calc = new WinCalculator(cards);
        const outcome = calc.getBestHand();

        this.setState({cards, outcome});
    };

    render() {
        return (
            <div className="App playingCards faceImages cards">
                <div>
                    <button onClick={this.shuffleCards}>Shuffle cards</button>
                </div>
                <ul className="table ">
                    {
                        this.state.cards.map((card, index) => {
                            return (
                                <li key={index} >
                                    <Card suit={card.suit} rank={card.rank} />
                                </li>
                            )

                        })
                    }
                </ul>
                <div style={{marginTop: '20px'}}>
                    {this.state.outcome}
                </div>

            </div>
        );
    }
}

export default App;
