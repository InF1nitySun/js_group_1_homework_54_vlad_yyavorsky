import WinCalculator, {OUTCOMES} from './WinCalculator'

const royalFlush = [
    {suit:'H', rank: 'Q'},
    {suit:'H', rank: 'A'},
    {suit:'H', rank: 'J'},
    {suit:'H', rank: 'K'},
    {suit:'H', rank: '10'},
];

const flush = [
    {suit:'H', rank: '2'},
    {suit:'H', rank: 'A'},
    {suit:'H', rank: '3'},
    {suit:'H', rank: 'K'},
    {suit:'H', rank: '10'},
];

const pair = [
    {suit:'H', rank: '2'},
    {suit:'C', rank: 'A'},
    {suit:'S', rank: '2'},
    {suit:'S', rank: 'K'},
    {suit:'D', rank: '10'},
];

it('should determine royal flush', () => {
    const calc = new WinCalculator(royalFlush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.ROUYAL_FLUSH);
});

it('should determine flush', () => {
    const calc = new WinCalculator(flush);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.FLUSH);
});

it('should determine a pair', () => {
    const calc = new WinCalculator(pair);
    const result = calc.getBestHand();

    expect(result).toEqual(OUTCOMES.PAIR);
});