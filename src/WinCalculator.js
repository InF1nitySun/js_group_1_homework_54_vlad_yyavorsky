export const OUTCOMES = {
    ROUYAL_FLUSH: 'Royal Flush',
    FLUSH: 'Flush',
    PAIR : 'Pair',
    NOTHING: 'Nothing'
};

class WinCalculator {
    constructor(cards) {
        this.cards = cards;

        this.suits = this.cards.map(card => card.suit);
        this.ranks = this.cards.map(card => card.rank);

        this.isFlush = this.suits.every(suit => suit === this.suits[0]);
    }

    isRoyalFlush() {
        return this.isFlush &&
            this.ranks.includes('10') &&
            this.ranks.includes('J') &&
            this.ranks.includes('Q') &&
            this.ranks.includes('K') &&
            this.ranks.includes('A')
    }

    isPair() {
        const ranksNumber = {};

        this.ranks.forEach(rank => {
            if (!ranksNumber[rank]) {
                ranksNumber[rank] = 1;
            } else {
                ranksNumber[rank]++;
            }
        });
        return Object.values(ranksNumber).includes(2);

    }


    getBestHand() {
        if (this.isRoyalFlush()) {
            return OUTCOMES.ROUYAL_FLUSH;
        } else if (this.isFlush) {
            return OUTCOMES.FLUSH;
        } else if (this.isPair()) {
            return OUTCOMES.PAIR;
        } else {
            return 'Nothing';
        }
    }

}

export default WinCalculator